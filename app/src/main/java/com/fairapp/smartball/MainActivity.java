package com.fairapp.smartball;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private static Toolbar toolbar;
    private ListViewAdapter adapter;
    private ListViewAdapter2 adapter2;
    private ListView list;

    public int number = 0;
    TextView seektxt;
    SeekBar sb;

    LinearLayout tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("TAG","orCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources r = Resources.getSystem();
        Configuration config = r.getConfiguration();
        onConfigurationChanged(config);

        setINITSETTINGS(); //앱 초기 실행시 화면 관련 초기설정

        setDataSettings(); //백단 데이터 초기 세팅

    }

    @Override public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        View LoadPage = findViewById(R.id.ID_LAYOUT);

        switch (newConfig.orientation){
            case Configuration.ORIENTATION_LANDSCAPE:
                LoadPage.setBackgroundResource(R.drawable.bg_cat);
                Log.d("Orientation","Landscape:bg_cat");
                break;

            case Configuration.ORIENTATION_PORTRAIT:
                LoadPage.setBackgroundResource(R.drawable.bg_golden_retriver);
                Log.d("Orientation","Portrait:bg_golden_retriever");
                break;
        }
        /*if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){ // 배경 화면 교체 처리
            setContentView(R.layout.activity_main);
        }else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){ // 배경 화면 교체 처리
            setContentView(R.layout.activity_landscape_main);
        }*/
    }


    // 단말기 해상도 얻기
    private void getDisplayInfo(){
        // px
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int deviceWidth = displayMetrics.widthPixels;
        int deviceHeight = displayMetrics.heightPixels;
        Log.d("TAG","displayMetrics.density"+displayMetrics.density);
        Log.d("TAG","width(px)=>>>"+deviceWidth+",height(px)=>>>"+deviceHeight);


        // dp
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
        // constant 값: DENSITY_LOW 120, DENSITY_MEDIUM 160, DENSITY_HIGH 240

        int density = metrics.densityDpi;
        Log.d("TAG","densityDpi : " + density);

        int dipWidth = (int) (deviceWidth / displayMetrics.density);
        int dipHeight = (int) (deviceHeight / displayMetrics.density);
        Log.d("TAG","dipWidth(dp) : " + dipWidth + ", dipHeight(dp) : " + dipHeight);
    }

    // Deviceinfo 레이아웃 너비, 높이 얻기
    private void getDeviceInfo(){
        tv = (LinearLayout)findViewById(R.id.Device_info);
        int Dwidth = tv.getWidth();
        int Dheight = tv.getHeight();
        Log.d("TAG","x=>>>"+Dwidth+",y=>>>"+Dheight);
    }

    // PX ---> DP
    public static int getPixelToDp(Context context, int pixel){
        float dp = 0;
        try {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            dp = pixel / (metrics.densityDpi / 160f);
        } catch (Exception e) {

        } return (int) dp;
    }

    // DP ---> PX
    public static int getDpToPixel(Context context, int DP){
        float px = 0;
        try {
            px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DP,
                    context.getResources().getDisplayMetrics());
        } catch (Exception e) {

        } return (int) px;
    }


    //TransparentLayout 너비, 높이 설정
    private void setTransparent(){

    }

    private void setINITSETTINGS() {
        // 툴바 관련
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // 리스트뷰
        list = (ListView)findViewById(R.id.listitem_data);

        getDisplayInfo();
        getDeviceInfo();
    }

   
    public static void setToolbarTitle(String title) {
        toolbar.setTitle(title);

    }


    private void setDataSettings() {

        ListViewItem2.INIT();
        // ====================================================================================================
        // 1.Adapter 생성
        adapter2 = new ListViewAdapter2(); //여기 이부분이 여기서 어뎁터를 생성 하면서

        // 2.Adapter 연결
        list.setAdapter(adapter2);

        // 3.Adapter 아이템추가
//        adapter.addItem("2019-03-04","65","32","7");
//        adapter.addItem("2019-03-05","57","33","10");
//        adapter.addItem("2019-03-08","68","42","13");
//        adapter.addItem("2019-03-09","32","50","16");
//        adapter.addItem("2019-03-12","80","60","22");

        // 해봐 됨 이게 좋은게
        // ====================================================================================================
        //방법 2
/*
        ArrayList<ListViewItem> listViewItemList = new ArrayList<>();
        listViewItemList.add(new ListViewItem("2019-03-04","activity1","touch1","bite1"));
        listViewItemList.add(new ListViewItem("2019-03-04","activity1","touch1","bite1"));
        listViewItemList.add(new ListViewItem("2019-03-04","activity1","touch1","bite1"));
        listViewItemList.add(new ListViewItem("2019-03-04","activity1","touch1","bite1"));

        adapter = new ListViewAdapter(listViewItemList); //여기 이부분이 여기서 어뎁터를 생성 하면서
        list.setAdapter(adapter);
*/

        // ====================================================================================================




        // SeekBar 셋팅
        seektxt = (TextView)findViewById(R.id.seekbar_num);
        sb = (SeekBar) findViewById(R.id.seekbar);

        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //SeekBar의 상태가 변경되엇을 때 실행될 사항. progress는 seekbar의 상태 값을 가짐
                number = seekBar.getProgress();
                update();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //SeekBar의 움직임이 멈추면 실행될 사항
                number = seekBar.getProgress();

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //SeekBar의 움직임이 시작될 때 실행될 사항
                number = seekBar.getProgress();
            }
        });


        // 현재 시간 표시(activity_main)
        TextView tv = findViewById(R.id.date);
        tv.setText(getTime());
    }

    public void update(){
        //SeekBar의 값에 따라 TextView로 보여주고 화면을 바꾸기 위해 정의한 함수.
        seektxt.setText(new StringBuilder().append(number));
    }


    private String getTime(){
        Calendar cal = Calendar.getInstance();
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT);
        formatter.setTimeZone(cal.getTimeZone());
        String formatted = formatter.format(cal.getTime());
        String tot_str = formatted + "\n";

        return tot_str;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //return super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.menu_scan:
                Intent intent = new Intent(this,DeviceScanActivity.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Smart ball Scan", Toast.LENGTH_SHORT).show();
                break;

           /* case R.id.menu_stop:
                Toast.makeText(getApplicationContext(), "스캐닝 중지", Toast.LENGTH_LONG).show();
                break;*/
        }
        return true;
    }


}
