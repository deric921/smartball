package com.fairapp.smartball;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class LandscapeMainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("TAG","orCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landscape_main);
        getDisplay();
    }

    private void getDisplay(){
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        //--- Surface.ROTATION_0  : 세로
        // --- Surface.ROTATION_90  : 가로
        // --- Surface.ROTATION_180 : 세로
        // --- Surface.ROTATION_270 : 가로
        int rotation = display.getRotation();
    }
}
