/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fairapp.smartball;


import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
// UUID : 범용 고유 번호( Universally Unique IDentifiers)
//       범용적으로 사용할 수 있는 고유의 ID를 사용하기 위해 생성.
//       블루투스에서는 Device에서 제공하는 service를 검색하여 각 service마다 UUID를 부여하는 등 많은 부분에서 사용됨.

/*
    UUID의 구성요소
    UUID = (time_low) - (time_mid) - (time_high_and_version) - (clock_seq_hi_and_reserved) - (clock_seq_low_node)
*/



public class SampleGattAttributes {
    private static HashMap<String, String> attributes = new HashMap();
    public static String DESC = "00002902-0000-1000-8000-00805f9b34fb";
    public static String RX = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
    public static String TX = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
    public static String UART = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";


    static {
        //uart ServiceUUID
        attributes.put("6e400001-b5a3-f393-e0a9-e50e24dcca9e","uart Service");
        //Characteristics.
        attributes.put("6e400002-b5a3-f393-e0a9-e50e24dcca9e", "TX");
        attributes.put("6e400003-b5a3-f393-e0a9-e50e24dcca9e","RX");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
