package com.fairapp.smartball;

import java.util.ArrayList;

public class ListViewItem2 {

    public String date;
    public String activity;
    public String bite;
    public String touch;

    public static ArrayList<ListViewItem> arrayList;

    public static void INIT() {
        arrayList = new ArrayList<>();
        arrayList.add(new ListViewItem("2019-03-04","101","41","60"));
        arrayList.add(new ListViewItem("2019-03-04","58","20","38"));
        arrayList.add(new ListViewItem("2019-03-06","77","32","45"));
        arrayList.add(new ListViewItem("2019-03-07","62","15","47"));
        arrayList.add(new ListViewItem("2019-03-08","90","21","69"));
        arrayList.add(new ListViewItem("2019-03-08","90","21","69"));
        arrayList.add(new ListViewItem("2019-03-09","65","17","48"));
        arrayList.add(new ListViewItem("2019-03-11","104","34","70"));
    }

    //한눈에 보이 잖아 여기다 데이터 집어 넣기만 하면 리스트에 자동으로 들어감
    //어떤 리스트 던지 엑티비티던지 서비스던지 안가림
    //서비스에서 데이터 받아서 여기다 집어 넣으면 걍 엑티비티에서도 다 보임
    //엑티비티에서 데이터 넣고 서비스에서 쓸수도 있음 그냥 모든 상황을 다 뚫고 데이터가 돌아다님 아까같은건 한계가있죠? 당연히 엑티비티에서 엑티비티 밖에 못가고

    //다른 데서 넣으려면
    //ListViewItem2.arrayList.add(new ListViewItem("2019-03-04","activity1","touch1","bite1")); 맨앞에 클래스 이름만 적어주고 접근하면 됨 ㅠ
}
