package com.fairapp.smartball;

import java.util.List;

public class ListViewItem {

    public String date;
    public String activity;
    public String bite;
    public String touch;

    //작년 12월 기준 안드로이드 공식 권장 사항 -> 겟셋 쓰지 말것 (써도 되는데 구글 권장사항으로 올라옴)

    ListViewItem() {

    }


    ListViewItem(String date, String activity, String bite, String touch) {

       this.date = date;
        this.activity = activity;
        this.bite = bite;
        this.touch = touch;
    }

    public void setDate(String date){
       this.date = date;
    }

    public String getDate(){
        return date;
    }

    public void setActivity(String activity){
        this.activity = activity;
    }

    public String getActivity(){
        return activity;
     }

    public void setBite(String bite){
        this.bite = bite;
    }

    public String getBite(){
        return bite;
    }

    public void setTouch(String touch){
        this.touch = touch;
    }

    public String getTouch(){
        return touch;
    }


}
