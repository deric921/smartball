package com.fairapp.smartball;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DeviceScanActivity extends AppCompatActivity {
    private final static String TAG = "DeviceScanActivity";

    private LeDeviceListAdapter mLeDeviceListAdapter; // 스캔 리스트 어댑터
    private BluetoothAdapter mBluetoothAdapter; // 블루투스 어댑터
    private boolean mScanning; // 스캐닝 확인 값
    private Handler mHandler;  // handler
    private ListView listView;
    //private boolean isBluetooth;

    private static Toolbar toolbar;

    private static final int REQUEST_ENABLE_BT = 1;

    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000; // 스캔 주기

    //BLE Scanner 추가
    private BluetoothLeScanner mBLEScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        // 툴바 관련
        toolbar = (Toolbar)findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        listView = (ListView)findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
                if (device == null) return;
                final Intent intent = new Intent(DeviceScanActivity.this, DeviceControlActivity.class); // DeviceScanActivity > DeviceControlActivity
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
                intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());

                if (mScanning) {
                    //mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    mBLEScanner.stopScan(mScanCallback);
                    mScanning = false;
                }
                //startActivity(intent);

                MainActivity.setToolbarTitle(device.getName());
                startActivityForResult(intent,0);
            }
        });
        //

        //getActionBar().setTitle("Test");// 액션바에 BLE Device Scan
        mHandler = new Handler(); // 핸들러 객체 생성

        // 블루투스가 BLE를 지원하는 지 검사하는 로직
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.v(TAG,"블루투스를 지원하는지 확인");
            Toast.makeText(this, "BLE is not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        // 기기가 api 18이하일때
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Toast.makeText(this, "안드로이드의 버전이 낮습니다.", Toast.LENGTH_SHORT).show();
            finish();
        }

        // 블루투스 어댑터 생성. API 18 이상 가능
        // BluetoothManager를 통해 BluetoothAdapter를 얻고 BluetoothAdapter 초기화
        final BluetoothManager bluetoothManager =  (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

        mBluetoothAdapter = bluetoothManager.getAdapter();

       // 디바이스가 블루투스 지원 가능한 장치인지 확인
        if (mBluetoothAdapter == null) {
            Log.v(TAG,"디바이스에서 블루투스를 지원하지 않음");
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // 블루투스가 사용가능 상태인지 체크하고 아니라면 그 기능을 키도록 함.
        // App 중지없이 시스템 설정을 통한 블루투스 활성화 요청
        if (!mBluetoothAdapter.isEnabled()) {
            Log.v(TAG,"현재 기기의 블루투스 기능이 비활성화 상태입니다. 활성화시켜주세요.");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            startBluetooth();
        }

 /*       Intent intent = new Intent(DeviceScanActivity.this, BluetoothLeService.class);
        startService(intent);
*/

    }

    private void startBluetooth() {

        mBLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
        // Checks if Bluetooth LE Scanner is available.
        if (mBLEScanner == null) {
            Log.v(TAG,"BLE장치가 없습니다.");
            Toast.makeText(this, "Can not find BLE Scanner", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        // list view adapter를 생성
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        listView.setAdapter(mLeDeviceListAdapter);
        //setListAdapter(mLeDeviceListAdapter);
        Log.v(TAG,"어댑터 생성.");

        scanLeDevice(true);
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        } else if(requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK) {
            Toast.makeText(this, "블루투스를 활성화합니다.",Toast.LENGTH_SHORT).show();
            startBluetooth();
        }
        finish();
//        super.onActivityResult(requestCode, resultCode, data);
    }

    // 스캐닝을 통한 찾은 디바이스를 담을 어댑터
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = DeviceScanActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if(!mLeDevices.contains(device)) {
                Log.v(TAG,"스캐닝된 Device를 리스트에 추가");
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                Log.v(TAG,"스캐닝되어 리스트에 추가된 Device를 TextView로 뿌림");
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            viewHolder.deviceAddress.setText(device.getAddress());

            return view;
        }
    }

    private void scanLeDevice(final boolean enable) {

        if (enable) {
            Log.v(TAG,"스캔시작");
            // Stops scanning after a pre-defined scan period.
            // 스캔 주기가 지나면 스캔을 그만두게 함.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    //mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    mBLEScanner.stopScan(mScanCallback);
                    invalidateOptionsMenu(); // onCreateOptionsMenu 메소드를 다시 호출해줌.
                }
            }, SCAN_PERIOD);

            // 스캔 시작
            mScanning = true;
            //mBluetoothAdapter.startLeScan(mLeScanCallback);
            mBLEScanner.startScan(mScanCallback);
        } else {
            Log.v(TAG,"스캔멈춤");
            // 스캔 멈춤.
            mScanning = false;
            //mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mBLEScanner.stopScan(mScanCallback);
        }
        invalidateOptionsMenu();
    }


/*    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLeDeviceListAdapter.addDevice(device);
                            mLeDeviceListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            };*/

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override        public void onScanResult(int callbackType, ScanResult result) {
            processResult(result);
        }

        @Override        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                processResult(result);
            }
        }

        @Override        public void onScanFailed(int errorCode) {
        }

        private void processResult(final ScanResult result) {
            runOnUiThread(new Runnable() {
                @Override                public void run() {
                    Log.v(TAG,"스캐닝된 Device의 rssi");
                    mLeDeviceListAdapter.addDevice(result.getDevice());
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }
            });
        }
    };


//    @Override
//    protected void onListItemClick(ListView l, View v, int position, long id) {
//        final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
//        if (device == null) return;
//        final Intent intent = new Intent(this, DeviceControlActivity.class);
//        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME, device.getName());
//        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
//        if (mScanning) {
//            //mBluetoothAdapter.stopLeScan(mLeScanCallback);
//            mBLEScanner.stopScan(mScanCallback);
//            mScanning = false;
//        }
//        startActivity(intent);
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_scan, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                mLeDeviceListAdapter.clear();
                scanLeDevice(true);
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                break;
        }
        return true;
    }


    @Override
    protected void onPause() {
        super.onPause();
        //scanLeDevice(false);
        //mLeDeviceListAdapter.clear();

    }


    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }
}

