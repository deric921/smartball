package com.fairapp.smartball;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {

    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<ListViewItem> listViewItemList;
    // ListViewAdapter 생성자
    public ListViewAdapter(){
        listViewItemList = new ArrayList<>(); //여기 인데 리스트도 같이 생성 되는거지 형 그럼 저 이거 ListViewItem 요거 써먹을려면 전역변수 선언해줘야하는거아녀여?? 정확히는 아이템을 꺼내는게 아니라 아이템이 들어있는 리스트만 전역으로 쓰면 되지 아이템 자체는 그냥 데이터를 담는 그릇이고 그 데이터를 보관 하는 그거만 전역으로 써도 됨 그냥 이건 내 스타일이고 개발자 맘임
    }
    public ListViewAdapter(ArrayList<ListViewItem> listViewItemList) {
        this.listViewItemList = listViewItemList;
    }

    // Adapter 에 사용되는 데이터의 개수를 리턴. : 필수 구현
    @Override
    public int getCount() {
        return listViewItemList.size();
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return listViewItemList.get(position);
    }

    // 지정한 위치(position)에 있는 데이터와 관계된 아이템(row)의 ID를 리턴. : 필수 구현
    @Override
    public long getItemId(int position) {
        return position;
    }

    // position에 위치한 데이터를 화면에 출력하는데 사용될 View를 리턴. : 필수 구현
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        // "listitem_data" Layout을 inflate하여 convertView 참조 획득.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem_data, parent, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
        TextView dateTextView = (TextView) convertView.findViewById(R.id.date_log);
        TextView activityTextView = (TextView) convertView.findViewById(R.id.activity);
        TextView biteTextView = (TextView) convertView.findViewById(R.id.bite) ;
        TextView touchTextView = (TextView) convertView.findViewById(R.id.touch) ;

        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        ListViewItem listViewItem = listViewItemList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        dateTextView.setText(listViewItem.getDate());
        activityTextView.setText(listViewItem.getActivity());
        biteTextView.setText(listViewItem.getBite());
        touchTextView.setText(listViewItem.getTouch());

        return convertView;
    }

    // 아이템 데이터 추가를 위한 함수. 개발자가 원하는대로 작성 가능.
    public void addItem(String date, String activity, String touch, String bite){
        //이렇게 하면 코드 한줄
        listViewItemList.add(new ListViewItem(date,activity,touch,bite));
    }
}
